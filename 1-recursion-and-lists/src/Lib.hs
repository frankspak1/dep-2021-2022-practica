{-|
    Module      : Lib
    Description : Checkpoint voor V2DEP: recursie en lijsten
    Copyright   : (c) Brian van de Bijl, 2020
    License     : BSD3
    Maintainer  : nick.roumimper@hu.nl

    In dit practicum oefenen we met het schrijven van simpele functies in Haskell.
    Specifiek leren we hoe je recursie en pattern matching kunt gebruiken om een functie op te bouwen.
    LET OP: Hoewel al deze functies makkelijker kunnen worden geschreven met hogere-orde functies,
    is het hier nog niet de bedoeling om die te gebruiken.
    Hogere-orde functies behandelen we verderop in het vak; voor alle volgende practica mag je deze
    wel gebruiken.
    In het onderstaande commentaar betekent het symbool ~> "geeft resultaat terug";
    bijvoorbeeld, 3 + 2 ~> 5 betekent "het uitvoeren van 3 + 2 geeft het resultaat 5 terug".
-}

-- bronnen: https://www.youtube.com/watch?v=02_H3LjqMr8&list=PLhx8v4mr6oU_nAJcsSWe4qrHeON7vG8Ov&index=7&t=1s&ab_channel=DerekBanas
-- stackoverflow.com
-- https://hackage.haskell.org/package/CheatSheet-1.11/src/CheatSheet.pdf

module Lib
    ( ex1, ex2, ex3, ex4, ex5, ex6, ex7
    ) where


-- | ex1 is een functie die de som van een lijst getallen berekend
ex1 :: [Int] -> Int
ex1 [] = 0
ex1 (x:xs)= x + ex1 xs
-- ^ in deze functie worden de getallen in de lijst steeds bij elkaar op geteld --> [a + b + c.. etc]


-- | ex2 is een functie die elk element in de lijst verhoogt met 1
ex2 :: [Int] -> [Int]
ex2 (xs) = [x + 1| x <- xs]
-- ^ deze functie itereert over de lijst en verhoogt alle elementen met +1 --> [ (a+1), (b+1), (c+1).. etc]


-- | ex3 is een functie die alle elementen van de lijst vermenigvuldigt met -1
ex3 :: [Int] -> [Int]
ex3 (xs) = [x * (-1)| x <- xs]
-- ^ bij ex3 wordt er over de lijst geitereerd en elk element vermenigvuldigt met -1 --> [ (a * -1), (b * -1), (c * -1).. etc]


-- | ex4 is een functie waar er twee lisjten aan elkaar worden geplakt
ex4 :: [Int] -> [Int] -> [Int]
ex4 xs [] = xs
ex4 [] ys = ys
ex4 xs ys = head xs : ex4 (tail xs) ys
-- ^ head geeft het eerste element van de lijst terug en tail geeft het laatste element van de lijst terug
-- ^ doormiddel van recursie loopt die alleen door de lijst van xs en plakt die de lijst ys er achter
-- ^ het begint dus bij het begin van de lijst xs en dan doormiddlen van : voegt die vanaf het laatste element van xs de lijst ys toe


-- | ex5 is een functie die twee lijsten van gelijke lengte paargewijs bij elkaar optelt
ex5 :: [Int] -> [Int] -> [Int]
ex5 xs [] = xs
ex5 [] ys = ys
ex5 (x:xs) (y:ys) = (x + y) : ex5 xs ys
-- ^  bij deze functie worden de indexen van de twee lijsten bij elkaar opgeteld --> (xs[0] + ys[0]) , (xs[1] + ys[1]) etc


-- | ex6 is een functie die de twee lijsten van gelijke lengte paargewijs met elkaar vermenigdvuldigt
ex6 :: [Int] -> [Int] -> [Int]
ex6 xs [] = xs
ex6 [] ys = ys
ex6 (x:xs) (y:ys) = (x * y) : ex6 xs ys
-- ^ bij ex6 gebeurd hetzelfde als bij ex5 alleen dan wordt het vermenigvuldigt i.p.v. bij elkaar opgeteld --> xs[0] * ys[0]

-- | ex7 is een functie waarbij functies ex1 en ex6 bij elkaar gecombineerd worden tot een nieuwe functie die het inwendige product uitrekent
ex7 :: [Int] -> [Int] -> Int
ex7 x y = ex1 $ ex6 x y
-- ^ bij deze functie is het $ teken een infix operator ( bewerkings teken) dat de twee functies dus koppelt
-- ^ de werking van de functie ziet er als volgt uit --> [a,b,c] [x,y,z] -> (a*z) + (b*y) + (c*z) = d
